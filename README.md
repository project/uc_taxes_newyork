CONTENTS OF THIS FILE
---------------------
 
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module adds support for calculating New York Discretionary Surtax in Ubercart.
New YorkDiscretionary Surtax is added to the standard . 
The New York Discretionary Surtax amount is calculated by determining the delivery 
county (via zip code) of the order and adding that county surtax amount to the normal state tax

Base code is copied from : https://www.drupal.org/project/uc_taxes_floridasurtax

REQUIREMENTS
------------

"Ubercart"


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure module at : admin/store/settings/nytax
   
 * Update County surtax rate on : admin/store/settings/nytax/rate-form

MAINTAINERS
-----------

Current maintainers:
 * Abhishek Vishwakarma (visabhishek) - https://drupal.org/u/visabhishek